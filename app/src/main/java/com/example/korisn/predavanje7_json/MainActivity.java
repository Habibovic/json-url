package com.example.korisn.predavanje7_json;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {
    EditText enterText;
    TextView cityName;
    TextView curentWeather;
    TextView description;
    ImageView icon;
    ProgressBar barProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        enterText = (EditText) findViewById(R.id.enterText);
        cityName = (TextView) findViewById(R.id.grad);
        curentWeather = (TextView) findViewById(R.id.vrijeme);
        description = (TextView) findViewById(R.id.opis);
        icon = (ImageView) findViewById(R.id.icon);
        barProgress = (ProgressBar) findViewById(R.id.barProgress);
    }

    public void getWeather(View view) {

        String enterT = enterText.getText().toString();
        if (enterT.isEmpty()) {
            Toast.makeText(this, "Please enter City name", Toast.LENGTH_SHORT).show();
        } else {
            DownloadClass task = new DownloadClass();
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromInputMethod(cityName.getWindowToken(), 0);

            try {

                task.execute("http://api.openweathermap.org/data/2.5/weather?q=" + enterText.getText().toString() + "&APPID=bd5e378503939ddaee76f12ad7a97608").get();
                Toast.makeText(this, "Search weather for: " + enterT, Toast.LENGTH_SHORT).show();

            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }


    }

    public void resetInputText(View view) {
        enterText.setText("");
    }


    private class DownloadClass extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            barProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {
            String result = "";
            URL url;
            HttpURLConnection httpURLConnetctions = null;
            try {
                url = new URL(urls[0]);
                httpURLConnetctions = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnetctions.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                int data = inputStream.read();

                while (data != -1) {
                    char charater = (char) data;
                    result += charater;
                    data = inputStreamReader.read();
                }

                return result;

            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                JSONObject object = new JSONObject(s);
                String weather = object.getString("weather");
                JSONArray jsonArray = new JSONArray(weather);
                Log.i("Info", weather);
                for (int i = 0; i < 1; i++) {
                    JSONObject jsonPart = jsonArray.getJSONObject(i);

                    cityName.setText(object.getString("name"));
                    description.setText(jsonPart.getString("description"));
                    curentWeather.setText(jsonPart.getString("main"));
                    String icons = jsonPart.getString("icon");
                    Picasso.with(MainActivity.this)
                            .load("http://openweathermap.org/img/w/" + icons + ".png")
                            .placeholder(R.drawable.loading)
                            .error(R.drawable.noimagefound)
                            .into(icon);
                    barProgress.setVisibility(View.GONE);
                }

            } catch (JSONException e) {
                Toast.makeText(MainActivity.this, "Weather not found", Toast.LENGTH_LONG).show();
            }
        }
    }
}
